import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;


public class HeartBeat extends Thread{
	
	private boolean kill = false;
	public static String trackerIPAddress="";
	public static int trackerPortNum=-1;
	
	static void setTrackerInfo(String ip, int port){
		trackerIPAddress = ip;
		trackerPortNum = port;
	}
	
	public static int peerTCPPort=-1;
	
	static PeerInfo peerInfo=null; 
	
	// don't change it from volatile
	static volatile String peersList = "";
	
	static PeerInfo getPeerInfo(){
		return peerInfo;
	}
	
	public static void MakeMeBusy(){
		peerInfo.setPeerState(PeerInfo.State.Busy);
	}
	
	public static void MakeMeAvailable(){
		peerInfo.setPeerState(PeerInfo.State.Available);
	}
	
	public static String getPeerListAsString(){
		return peersList;
	}
	
	public static ArrayList<String> getPeerList(){
		ArrayList<String> list = new ArrayList<String>();
		
		for(String str: peersList.split(";")){
			if(! str.equals(""))
				list.add(str);
		}
		
		return list;
	}
	
	public void killHeartBeat(){
		kill = true;
	}
	
	public HeartBeat(TaskExecuter taskExecuter){
		peerTCPPort = taskExecuter.getTCPPort();
		maintainPeerList();
	}
	
	private void maintainPeerList(){
		Thread threadReadPeerListFromTracker = new Thread(){
			public void run(){
				//Create Datagram Socket
				DatagramSocket datagramSocket = null;
				try{
					datagramSocket = new DatagramSocket();
					
					if(peerInfo == null){
						InetAddress addr=null;
						try {
							addr = InetAddress.getLocalHost();
						} catch (UnknownHostException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						String ip = addr.getHostAddress();
						peerInfo = new PeerInfo(peerTCPPort, datagramSocket.getLocalPort(), ip );
					}
						
				}
				catch(SocketException exc){
					System.out.println("Exception creating socket: "+exc.getMessage());
					return;
				}
				
				//Receive Datagram packet
				byte[] buffer = new byte[5000];
				DatagramPacket datagramPacket = new DatagramPacket(buffer, buffer.length);
				while(true)
				{
					try{
						//Receive Data UDP Packet
						datagramSocket.receive(datagramPacket);
						
						//fetch data from UDP - Datagram
						byte[] data = datagramPacket.getData();
						
						String msg = new String(data);
						if(!msg.isEmpty()){
							peersList = msg;
						}
						
						//System.out.println("Available Peers: " + msg);
						
					}
					catch(IOException exc){
						System.out.println("Exception in receive: "+exc.getMessage());
						return;
					}
				}
			}
		};
		threadReadPeerListFromTracker.start();
		
	}
	
	private void sendHeartBeat()
	{
		//System.out.println("HB.");
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		
		try {
			out = new ObjectOutputStream(bos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		try {
			out.writeObject(peerInfo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] sendData = bos.toByteArray();
		  
		InetAddress address =null;
		DatagramSocket dgramSock=null;
		
		try{
			dgramSock = new DatagramSocket();
		}
		catch (SocketException exc){
			System.out.print("Error Creating Socket: "+exc.getMessage());
			return;
		}
		
		try{
			address = InetAddress.getByName(trackerIPAddress);
		}
		catch(UnknownHostException exc){
			System.out.println("Failed !"+ exc.getMessage());
			return;
		}
		DatagramPacket dgramPkt = new DatagramPacket(sendData, sendData.length, address, trackerPortNum);
		
		try{
			dgramSock.send(dgramPkt);
		}
		catch(IOException exc){
			System.out.println("Failed to Send: "+exc.getMessage());
			return;
		}
		
		dgramSock.close();
	}

	
	public void run() {
		while(!kill){
			
			try{
				Thread.sleep(1000);
			}
			catch(InterruptedException exc){
				System.out.println("Exception, sleep error");
			}
			
			//Send Heart Beat
			sendHeartBeat();
		}
		System.out.println("Mango");
	}
	
}
