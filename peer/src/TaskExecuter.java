import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TaskExecuter {
	
	private int TCPListenPort=-1;
	private ServerSocket sSocket = null;
	private Socket accSocket = null;
	private boolean isConnected = false;
	
	
	public TaskExecuter(){
		
		boolean sockCreated = false;
		int TCPport = 1000;
		while(! sockCreated){
			try{
				sSocket = new ServerSocket(TCPport);
				sockCreated = true;
				TCPListenPort = TCPport;
				System.out.println("TCP port Created: "+TCPport);
			}
			catch(IOException ex){
				++TCPport;
			}
		}
		
		/*
		 * listen on this port and accept it on accSocket
		 * in a thread, and after accepting , check and call execute Task();
		 */
		
		Thread acceptSocketThread = new Thread(){
			public void run(){
				while(true){
					try {
						System.out.println("Waiting for Connection");
						accSocket = sSocket.accept();
						isConnected = true;
						System.out.println("Connected to Peer: ");
						HeartBeat.MakeMeBusy();
						executeTask();
						HeartBeat.MakeMeAvailable();
					} catch (IOException e) {
						System.out.println("Failed to Accept Connection: "+e.getMessage());
					}
				}
			}
		};
		acceptSocketThread.start();
		
		System.out.println("Task Executor Instanciated ! on TCP Port: "+TCPListenPort);
	}
	
	public int getTCPPort(){
		return TCPListenPort;
	}
	
	public void executeTask(){
		System.out.println("Executing task on peer; ..... ");
		while(true){
			byte data[]=new byte[1024];
			try {
				if(accSocket.getInputStream().read(data) <1)
					break;
				
			} catch (IOException e) {
				System.out.println(e.getMessage());
				System.exit(1);
			}
			
			String strMessaget = new String(data);
			strMessaget = strMessaget.trim();
			System.out.println("received "+strMessaget);
		}
		/*
		 * read data from acc Socket
		 * just print it ,,
		 */
		
		//before return call sendData() to send the result
		
		// before exit / return , must close the accept socket,
		// let the listener socket run
	}
	
	private void sendData(){
		
	}
}