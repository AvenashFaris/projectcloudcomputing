import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class PeerApplication {

	public static void executeTask(String arg){
		System.out.println("Processing Request !");
		
		
		System.out.println();
		int peerCount = Integer.parseInt(arg.split(" ")[1]);
		
		JobTracker jobTracker = new JobTracker();
		jobTracker.updateAvailablePeerList();
		
		// Requested # of peers not available
		if(peerCount > jobTracker.getAvailablePeerCount()){
			System.out.println("Requested number of peers not available");
			System.out.println("Total Peers Available: "+jobTracker.getAvailablePeerCount());
			
			int i=1;
			for(String str: HeartBeat.getPeerList()){
				if(!str.equals("") && null!=str)
					System.out.println(i++ + " : "+str);
			}
			
		}
		else{
			if(jobTracker.selectPeers(peerCount)){
				System.out.println("Executing task on Peers");
				jobTracker.startTasksOnPeers();
			}
		}
		
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		FileReader fr=null;
		BufferedReader br=null;
		try {
			fr = new FileReader("config.txt");
			br = new BufferedReader(fr);
		} catch (FileNotFoundException e) {
			System.out.println("failed to read config.txt file: "+e.getMessage());
			return;
		}
		
		int port=-1;
		String ip="localhost";
		
		String line = "";
		try{
			while((line = br.readLine()) != null){
				String input[] = line.split("=");
				if(input[0].equals("ip"))
					ip = input[1];
				else if(input[0].equals("port"))
					port = Integer.parseInt(input[1]);
				}
				
				br.close();
				fr.close();
		}
		catch(IOException e){
			System.out.println("Error reading file: "+e.getMessage());
		} 
		
		HeartBeat.setTrackerInfo(ip, port);
		System.out.println("Looking for tracker : "+ ip + ":"+ port);
		TaskExecuter taksExecutor = new TaskExecuter();
		HeartBeat heartBeat = new HeartBeat(taksExecutor);
		heartBeat.start();
		String arg = new String();
		System.out.println("Appliation Started ...");
		
		Scanner sc = new Scanner(System.in);
		
		while(! arg.equals("exit")){
			System.out.println("--------------------------------------");
			System.out.println("Select Option: ");
			System.out.println("list - 		Show all peers in network");
			System.out.println("start <N> -	Start Job at N peers");
			System.out.println("exit - 		Start Job at N peers");
			
			arg = sc.nextLine();
			
			
			switch(arg){
			case "list":
				int i=1;
				for(String str: HeartBeat.getPeerList()){
					if(!str.isEmpty() && str.length() < 4000)
						System.out.println(i++ + " : "+str + " "+ str.length());
				}
				break;
			case "exit":
				System.out.println("Exiting Application");
				break;
			default:
				// Execute Task
				if(arg.startsWith("start ")){
					executeTask(arg);
				}
				// INVALID OPTION
				else{
					System.out.println("Invalid Option Selected !");
				}
			}
		}
		sc.close();
		System.exit(0);	// exit application
	}

}

