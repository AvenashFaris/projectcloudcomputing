import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class JobTracker {

	private ArrayList<PeerInfo> availablePeerList = null;
	
	private HashMap<String,Socket> sockMap = null;
	private HashMap<String,Socket> sockMapRetry = null;
	boolean triedAgain = false;
	
	public JobTracker() {
		sockMap = new HashMap<String, Socket>();
		sockMapRetry = new HashMap<String, Socket>();
	}
	
	public void updateAvailablePeerList(){
		String peerListAsString = HeartBeat.getPeerListAsString() ;
		if(null == peerListAsString || peerListAsString.isEmpty())
			return;
		
		ArrayList<PeerInfo> tempPeerList = new ArrayList<PeerInfo>();
		
		for(String str: peerListAsString.split(";")) {
			int port=-1;
			String ip="";
			boolean isIp = true;
			for(String s: str.split(",")){
				if(isIp){
					ip = s;
				}else{
					port = Integer.parseInt(s);
				}
				
				isIp= !isIp;
			}
			
			PeerInfo pi = new PeerInfo(port, -1, ip);
			tempPeerList.add(pi);
		}
		
		availablePeerList = tempPeerList;
	}
	
	public int getAvailablePeerCount(){
		updateAvailablePeerList();
		if(availablePeerList==null)
			return 0;
		else
			return availablePeerList.size(); 
	}
	
	public boolean selectPeers(int toSelect){
		if(toSelect > getAvailablePeerCount()){
			System.out.println("Requested # of peers not available ");
			return false;
		}
		else{
			//Select peers and add them in selectedPeerList
			// make connections
			makeConnections(toSelect, false);
		}
		return true;
	}
	
	private void makeConnections(int count, boolean isRetry){
		System.out.println("Requested "+count+ " connections");
		int connectionsAcquired = 0;
		getAvailablePeerCount();
		if(!isRetry){
			for(int i=0; i<availablePeerList.size(); ++i){
				try {
					String key =  availablePeerList.get(i).getIpAddress() + Integer.toString(availablePeerList.get(i).getTCPportNum());
					if(sockMap.containsKey(key))
						continue;
					System.out.println(i+ ": connecting ---------");
					Socket sock = new Socket(availablePeerList.get(i).getIpAddress(), availablePeerList.get(i).getTCPportNum());
					System.out.println("connecting: "+availablePeerList.get(i).getIpAddress() + ": "+availablePeerList.get(i).getTCPportNum());
					sockMap.put(key, sock);
					++connectionsAcquired;
					if(connectionsAcquired == count)
						break;
					
				} catch (UnknownHostException e) {
					System.out.println("Failed to connect: "+e.getMessage());
				} catch (IOException e) {
					System.out.println("Failed to connect: "+e.getMessage());
				}
			}
		} else {
			for(int i=0; i<availablePeerList.size(); ++i){
				try {
					String key =  availablePeerList.get(i).getIpAddress() + Integer.toString(availablePeerList.get(i).getTCPportNum());
					if(sockMapRetry.containsKey(key) || sockMap.containsKey(key))
						continue;
					System.out.println(i+ ": connecting ---------");
					Socket sock = new Socket(availablePeerList.get(i).getIpAddress(), availablePeerList.get(i).getTCPportNum());
					System.out.println("connecting: "+availablePeerList.get(i).getIpAddress() + ": "+availablePeerList.get(i).getTCPportNum());
					sockMapRetry.put(key, sock);
					
					++connectionsAcquired;
					if(connectionsAcquired == count)
						break;
					
				} catch (UnknownHostException e) {
					System.out.println("Failed to connect: "+e.getMessage());
				} catch (IOException e) {
					System.out.println("Failed to connect: "+e.getMessage());
				}
		}
	}
}

	
	public void startTasksOnPeers(){
		System.out.println("run task on peer");
		
		for(int i=0; i<10; ++i){
			ArrayList<String> sockToDelKeys = new ArrayList<String>();
			for(String key: sockMap.keySet()){
				Socket sock = sockMap.get(key);
				String message = "Hellwo WOrld";
				try {
					sock.getOutputStream().write(message.getBytes());
				} catch (IOException e) {
					System.out.println("Peer Lost: "+ e.getMessage());
					sockToDelKeys.add(key);
				}
			}
			
			int lostPeerCount = sockToDelKeys.size();
			if(lostPeerCount > 0){
				for(String key:sockToDelKeys){
					if(sockMap.get(key)!= null)
						try {
							sockMap.get(key).close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
					sockMap.remove(key);
				}
				
				
				if(triedAgain){
					continue;
				}
				System.out.println("Failed task(s) "+lostPeerCount + ", will retry later");
				
				makeConnections(lostPeerCount, true);
				
				Thread threadRunRetryTask = new Thread(){
				    public void run(){
				    	startRetryTasksOnPeers();
				    }
				  };
				  
				  threadRunRetryTask.start();
				  triedAgain = true;
			}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		for(Socket sock: sockMap.values()){
			try {
				sock.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		sockMap.clear();
		/*
		 * send control message to each peer in selectedpeerlist as follows
		 *  "CONTROL:START"
		 */
	}
	
public void startRetryTasksOnPeers(){
		System.out.println("run retry task on peer");
		System.out.println("Retrying failed tasks on "+ sockMapRetry.size() + " peers");
		for(int i=0; i<10; ++i){
			ArrayList<String> sockToDelKeys = new ArrayList<String>();
			for(String key: sockMapRetry.keySet()){
				Socket sock = sockMapRetry.get(key);
				String message = "Hellwo WOrld - Retry";
				try {
					sock.getOutputStream().write(message.getBytes());
				} catch (IOException e) {
					System.out.println("Peer Lost: "+ e.getMessage());
					System.out.println("Failed to run the task on peer: "+sock.getRemoteSocketAddress().toString());
					sockToDelKeys.add(key);
				}
			}
			
			for(String key: sockToDelKeys){
				try {
					sockMapRetry.get(key).close();
				} catch (IOException e) {
					
				}
				sockMapRetry.remove(key);
				
			}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		for(Socket sock: sockMapRetry.values()){
			try {
				sock.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		sockMapRetry.clear();
		/*
		 * send control message to each peer in selectedpeerlist as follows
		 *  "CONTROL:START"
		 */
	}
	
	public void runBackupTasksOnBackupPeers(){
		//send control message to backup peer list
	}

	public void broadcastMessage(String strData){
		// send strData to all peers | in a loop
	}
	
	public void sendMessage(int peerId, String strData){
		// send strData to only specified peerID
		//
		// if peerID is > 0 , send the message to  i'th element in Available list
		// if peerID is < 0 , send the message to  i'th element in backup peer list
	}
	
	public void sendData(int peerId, byte Data){
		//
		// will be decided and implemented later, 
		// 
		
	}

	public String receiveMessage(int peerId){
		//
		//create a new thread and receive the message and return from this function
		//
		return "";
	}
	
	public byte[] receiveData(int peerId){
		//
		//create a new thread and receive the message (convert to byte array) and return from this function
		//
		String s="";
		return s.getBytes();
	}
	
	public void closeAllSockets(){
		
	}
}
