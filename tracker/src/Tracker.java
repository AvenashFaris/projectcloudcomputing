
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Tracker {

	public static final int TRACKER_DGRAM_LISTENER_PORT = 9999;
	
	// Map of all peers <String(IP,PORT), PeerInfo>
	private static Map<String, PeerInfo> peerMap;
	
	//Main
	public static void main(String[] args) {
		
		peerMap = new HashMap<String, PeerInfo>();
		
		//Create New thread to filter Disconnected Peers
		 Thread threadFilterDisconnectedPeers = new Thread(){
			    public void run(){
			    	removeDisconnectedPeers();
			    }
			  };
		 threadFilterDisconnectedPeers.start();
		//-----------------------------------------------
		 
		//Create New thread to filter Disconnected Peers
		 Thread threadBroadcastIpsToAllPeers = new Thread(){
			    public void run(){
			    	broadcastIpsToAllPeers();
			    }
			  };
		 threadBroadcastIpsToAllPeers.start();
		//-----------------------------------------------
		 
		 
		 System.out.println("Creating Dgram Sock");
		//Create Datagram Socket
		DatagramSocket datagramSocket = null;
		try{
			datagramSocket = new DatagramSocket(TRACKER_DGRAM_LISTENER_PORT);
		}
		catch(SocketException exc){
			System.out.println("Exception creating socket: "+exc.getMessage());
			return;
		}
		
		//Receive Datagram packet
		byte[] buffer = new byte[1000];
		DatagramPacket datagramPacket = new DatagramPacket(buffer, buffer.length);
		while(true)
		{
			try{
				//Receive Data UDP Packet
				datagramSocket.receive(datagramPacket);
				
				//Extract IP and Port
				int port = datagramPacket.getPort();
				String Address = datagramPacket.getAddress().getHostName();
				
				PeerInfo peerInfo = null;
				
				//fetch data from UDP - Datagram
				byte[] data = datagramPacket.getData();
				
				ByteArrayInputStream bais = new ByteArrayInputStream(data);
				ObjectInput oi = null;
				
				oi = new ObjectInputStream(bais);
				try {
					peerInfo = (PeerInfo) oi.readObject();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//System.out.print("HB-");
				updatePeerMap(peerInfo);
				
			}
			catch(IOException exc){
				System.out.println("Exception in receive: "+exc.getMessage());
				return;
			}
		}
		
	}
	
	
	public static void updatePeerMap(PeerInfo pInfo){
		String key = pInfo.getIpAddress() + Integer.toString(pInfo.getTCPportNum());
		if(!peerMap.containsKey(key)){
			System.out.println("Peer: "+pInfo.getIpAddress()+", TCP Port:"+pInfo.getTCPportNum() +", UDP Port:"+pInfo.getUDPportNum() +" Joined");
		}
		
		peerMap.put(key, pInfo);
		
	}

	public static void removeDisconnectedPeers(){
		while(true){
			// Sleep : 3 seconds
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
			try{
				//Thread issue here
				for (PeerInfo pInfo : peerMap.values()) {
					int stableCount = pInfo.getStableCount();
					if(stableCount==0){
						System.out.println("Peer Lost: "+pInfo.getIpAddress()+", TCP Port:"+pInfo.getTCPportNum() +", UDP Port:"+pInfo.getUDPportNum());
						String key = pInfo.getIpAddress() + pInfo.getTCPportNum();
						peerMap.remove(key);
						
					}
					else{
						pInfo.decrementStableCount();
					}
				}
			}
			catch(Exception e){
				
			}
		}
	}
	
	
	//Broadcast IP addresses to all peers
	public static void broadcastIpsToAllPeers(){
		while(true){
			// Sleep : 3 seconds
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(peerMap.isEmpty())
				continue;
			
			String peerInfoList="";
			
			//Create List of Peers : stable count > 1
			for (PeerInfo pInfo : peerMap.values()) {
				
				int stableCount = pInfo.getStableCount();
				
				if(stableCount>1){
					peerInfoList +=  (pInfo.getIpAddress()+","+pInfo.getTCPportNum()+";");
				}
			}
			
			
			byte[] sendData = peerInfoList.getBytes();
			InetAddress address = null;
			DatagramSocket dgramSock = null;
			//System.out.println("Creating Dgram Sock 2");
			
			try{
			//send list to all peers : stable count > 1
				for (PeerInfo pInfo : peerMap.values()) {
				
					try{
						dgramSock = new DatagramSocket();
					}
					catch (SocketException exc){
						System.out.print("Error Creating Socket: "+exc.getMessage());
						return;
					}
					
					int stableCount = pInfo.getStableCount();
					
					
					if(stableCount>1 && (pInfo.getPeerState() == PeerInfo.State.Available)){
						
						try {
							address = InetAddress.getByName(pInfo.getIpAddress());
						} catch (UnknownHostException e) {
							// TODO Auto-generated catch block
							//e.printStackTrace();
						}
						
						DatagramPacket dgramPkt = new DatagramPacket(sendData, sendData.length, address, pInfo.getUDPportNum() /*pInfo.getPort() -1*/);
						
						try {
							dgramSock.send(dgramPkt);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							//e.printStackTrace();
						}
						
					}
					
					dgramSock.close();	
				}
			}
			catch(Exception ex){
				
			}
			
		}
	}
}
