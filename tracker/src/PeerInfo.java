
import java.io.Serializable;

import com.sun.corba.se.spi.activation.TCPPortHelper;

public class PeerInfo implements Serializable{

	/**
	 * State of the Peer
	 * Available or Not Available
	 * 
	 * @author faris
	 *
	 */
	public enum State{
		Available,
		Busy,
		NotAvailable
	}
	
	/**
	 * ipAddress of the peer
	 */
	private String ipAddress;
	
	/**
	 * stable count
	 */
	private int stableCount=3;
	
	/**
	 * TCP portNum Port Number at which the peer is listening
	 */
	private int TCPportNum=-1;
	
	/**
	 * UDP portNum Port Number at which the peer is listening
	 */
	private int UDPportNum=-1;
	
	private State peerState;
	
	/**
	 * Constructor PeerInfo
	 * @param peerId sequence ID of the peer
	 * @param portNum Port Number at which the peer is listening
	 * @param ipAddress IP Address of the Peer
	 */
	public PeerInfo(int TCP_PortNum, int UDP_PortNum, String ipAddress){
		this.TCPportNum = TCP_PortNum;
		this.UDPportNum = UDP_PortNum;
		this.ipAddress = ipAddress;
		this.peerState = State.Available;
	}
	
	@Override
	public String toString() {
		return ("toString(): TCP Port:" +Integer.toString(this.TCPportNum)+ " UDP Port:" +Integer.toString(this.UDPportNum)+ ", IP:" + this.ipAddress + "\n");
	}
	
	// Private - non-accessible constructor
	private PeerInfo(){}
	
	public String getIpAddress(){
		return ipAddress;
	}
	
	public int getStableCount(){
		return stableCount;
	}
	
	public void decrementStableCount(){
		--stableCount;
	}

	public int getTCPportNum() {
		return TCPportNum;
	}

	public void setTCPportNum(int tCPportNum) {
		TCPportNum = tCPportNum;
	}

	public int getUDPportNum() {
		return UDPportNum;
	}

	public void setUDPportNum(int uDPportNum) {
		UDPportNum = uDPportNum;
	}
	
	public State getPeerState(){
		return this.peerState;
	}
	
	public void  setPeerState(State state){
		this.peerState = state;
	}
	
}
